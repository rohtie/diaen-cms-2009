<?php
echo '<div id="right">';
echo '<div id="header">Image</div>';
echo '<div id="content">';

//open directory
$directory = dir("bilder");
//scan directory
while ($entry = $directory->read()) {
	//allow extensions
	if (
	preg_match("/(\.gif$)/i", $entry)	||
	preg_match("/(\.bmp$)/i", $entry)	||
	preg_match("/(\.png$)/i", $entry)	||
	preg_match("/(\.jpeg$)/i", $entry)	||
	preg_match("/(\.jpg$)/i", $entry)
	){
		//fill array
		$pics[] = 'bilder/'.$entry;
		$pics2[] = $entry;
	}
}
//close directory
$directory->close();

if($_GET['do'] == 'view'){
	//display
	for($i=0; $i < count($pics); $i++){
		//get image dimensions
		$img = getimagesize($pics[$i]);
		//display image
		echo '<div class="im_c" style="background-color: #bbb; border-bottom: 3px solid #555; margin-bottom: 2px;"><center /><a href="?type=image&do=delete&id='.$i.'">delete</a> | <a href="?type=image&do=generate&id='.$i.'">generate image code</a></center>
		<img src="'.$pics[$i].'" />
		width:'.$img[0].'px height:'.$img[1].'px<br />full path: /'.$pics[$i].'</div>';
	}
}elseif($_GET['do'] == 'delete'){
	if(isset($_GET['true'])){
		//delete image
		unlink($pics[$_GET['id']]);

		echo '<div id="msg">'.$pics[$_GET['id']].' was successfully deleted<br /><a href="?type=image&do=view">back</a></div>';
	}else{
		//deletion confirmation
		echo '<div id="msg">
		Are you sure you want to delete this picture?<br /><img src="'.$pics[$_GET['id']].'" /><br />
		<a href="edit.php?type=image&do=delete&id='.$_GET['id'].'&true=1">Yes</a> 
		<a href="edit.php?type=image&do=view">No</a>
		</div>';
	}
}elseif($_GET['do'] == 'generate'){
	if(!isset($_POST['submit'])){
		echo '<form method="post" action="?type=image&do=generate">
			<h2>Photo by:</h2>
				<input type="text" class="head" name="takenby" />
				<input id="submit" type="submit" value="submit" name="submit" />
				<input type="hidden" name="photo" value="'.$pics[$_GET['id']].'" />
				<img src="'.$pics[$_GET['id']].'" />
		</form>';
	}else{
		if(empty($_POST['takenby'])){
			$_POST['takenby'] = 'Unknown';
		}
		echo 'Generated code: <br /><textarea style="width: 500px;"><div class="im_c"><img src="/'.$_POST['photo'].'" /><br />
Photo: '.$_POST['takenby'].'</div></textarea>';
	}
}elseif($_GET['do'] == 'add'){
	echo '<center><applet name="jumpLoaderApplet"
			code="jmaster.jumploader.app.JumpLoaderApplet.class"
			archive="/bilder/upload/jumploader_z.jar"
			width="600"
			height="400" 
			mayscript>
		<param name="uc_uploadUrl" value="/bilder/upload/uploadHandler.php"/>
	</applet></center>';
}
echo '</div>';
echo '<div id="map"><ul>
<li class="b">Images</li>';
for($i=0; $i < count($pics); $i++){
	echo '<li>'.$pics2[$i].'<br />(<a href="edit.php?type=image&do=generate&id='.$i.'">Generate code</a>
	<a href="edit.php?type=image&do=update&do=delete&id='.$i.'">X</a>)
	</li>';
}
echo '</ul></div>';
?>
