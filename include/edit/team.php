<?php
	/***********************************
			     team
	***********************************/
function show_team_page($query){
	$res = mysql_query($query);
	$team = mysql_fetch_array($res);
	echo '<h1>(<a href="edit.php?type=team&do=edit&id='.$team['id'].'">Edit</a>
	<a href="edit.php?type=team&do=update&task=delete&id='.$team['id'].'">X</a>)</h1>';
	if(!empty($team['title'])){
		echo '<h1>'.$team['title'].'</h1>';
	}else{
		echo '<h1>'.$team['name'].'</h1>';
	}
	echo 
	'<table><tr><td width="100%">
	<table>';
	if(!empty($team['occupation'])){
		echo '<tr><td>Occupation:</td><td>'.$team['occupation'].'</td></tr>';
	}
	if(!empty($team['email'])){
		echo '<tr><td>Email:</td><td>'.$team['email'].'</td></tr>';
	}
	if(!empty($team['fax'])){
		echo '<tr><td>Fax:</td><td>'.$team['fax'].'</td></tr>';
	}
	if(!empty($team['phone'])){
		echo '<tr><td>Phone:</td><td>'.$team['phone'].'</td></tr>';
	}
	if(!empty($team['mobile'])){
		echo '<tr><td>Mobile:</td><td>'.$team['mobile'].'</td></tr>';
	}
	if(!empty($team['website'])){
		echo '<tr><td>Website:</td><td><a href="http://'.$team['website'].'" target="_blank">'.$team['website'].'</a></td></tr>';
	}
	if(!empty($team['image'])){
		echo '</table>
		</td><td>
		<img src="'.$team['image'].'" width="180" />
		</td></tr></table>';
	}else{
		echo '</table>
		</td><td>
		<img src="/bilder/no_pic.jpg" width="180" />
		</td></tr></table>';
	}
	echo n2p($team['cv']);
}
echo '<div id="right">';
echo '<div id="header">team</div>';
echo '<div id="content">';

// VIEW team
if($_GET['do'] == 'view'){
	if(!isset($_GET['id'])){
		//latest team
		show_team_page("SELECT * FROM team ORDER BY position ASC limit 1");
	}else{
		//selected team
		show_team_page("SELECT * FROM team WHERE id='".$_GET['id']."'");
	}

// ADD team
}elseif($_GET['do'] == 'add'){
	$id_r = mysql_fetch_array(mysql_query("SELECT id FROM team ORDER BY id DESC limit 1"));
	$id = $id_r['id']+1;
	echo '<form method="post" action="edit.php?type=team&do=update" enctype="multipart/form-data">
		<h2>Name:</h2>
			<textarea class="head" name="name"></textarea>
		<h2>Title:</h2>
			<textarea class="head" name="title"></textarea>
		<h2>Add Image:</h2>
			<input type="file" id="upload" name="image">
		<h2>Occupation:</h2>
			<textarea class="head" name="occupation"></textarea>
		<h2>Email:</h2>
			<textarea class="head" name="email"></textarea>
		<h2>Fax:</h2>
			<textarea class="head" name="fax"></textarea>
		<h2>Phone:</h2>
			<textarea class="head" name="phone"></textarea>
		<h2>Mobile:</h2>
			<textarea class="head" name="mobile"></textarea>
		<h2>Website:</h2>
			<textarea class="head" name="website"></textarea>
		<h2>CV:</h2>
			<textarea id="body" name="body"></textarea>
		<input id="submit" type="submit" value="submit" name="submit" />
		<input type="hidden" name="task" value="add" />
		<input type="hidden" name="id" value="'.$id.'" />
		</form>';

// EDIT team
}elseif($_GET['do'] == 'edit' && isset($_GET['id'])){
	$res = mysql_query("SELECT * FROM team WHERE id='".$_GET['id']."'");
	$row = mysql_fetch_array($res);
	//show edit form
	echo '<form method="post" action="edit.php?type=team&do=update" enctype="multipart/form-data">
	<h2>Name:</h2>
		<textarea class="head" name="name">'.$row['name'].'</textarea>
	<h2>Title:</h2>
		<textarea class="head" name="title">'.$row['title'].'</textarea>
	<h2>Add New Image:</h2>
		<input type="file" id="upload" name="image"> Remove current image: <input type="checkbox" name="rm_img" value="1" />
	<h2>Occupation:</h2>
		<textarea class="head" name="occupation">'.$row['occupation'].'</textarea>
	<h2>Email:</h2>
		<textarea class="head" name="email">'.$row['email'].'</textarea>
	<h2>Fax:</h2>
		<textarea class="head" name="fax">'.$row['fax'].'</textarea>
	<h2>Phone:</h2>
		<textarea class="head" name="phone">'.$row['phone'].'</textarea>
	<h2>Mobile:</h2>
		<textarea class="head" name="mobile">'.$row['mobile'].'</textarea>
	<h2>Website:</h2>
		<textarea class="head" name="website">'.$row['website'].'</textarea>
	<h2>CV:</h2>
		<textarea id="body" name="body">'.$row['cv'].'</textarea>
	<input id="submit" type="submit" value="submit" name="submit" />
	<input type="hidden" name="task" value="edit" />
	<input type="hidden" name="id" value="'.$row['id'].'" />
	</form>';

// UPDATE team
}elseif($_GET['do'] == 'update'){
	// UPDATE ADD
	if($_POST['task'] == 'add'){
		//get new position
		$gpos = mysql_fetch_array(mysql_query("SELECT position FROM team ORDER BY position DESC LIMIT 1"));
		$position = $gpos['position'] + 1;
		//image
	 	$image=$_FILES['image']['name'];
	 	if($image){
			//get name
	 		$filename = stripslashes($_FILES['image']['name']);
	  		$extension = getExtension($filename);
	 		$extension = strtolower($extension);
			//Verify
			if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){
				echo '<h1>Unknown extension!</h1>';
				$errors=1;
			}else{
				$image_name=time().'.'.$extension;
				$newname="bilder/".$image_name;
				$copied = copy($_FILES['image']['tmp_name'], $newname);
				if (!$copied){
					echo '<h1>Copy unsuccessfull!</h1>';
					$errors=1;
				}
				if(!$errors){
					include 'include/resize.php';
					$image = new SimpleImage();
					$image->load($newname);
					$image->resizeToWidth(180);
					$image->save($newname);

					mysql_query("INSERT INTO team
					(id, name, title, occupation, cv, email, fax, phone, mobile, website, image, position)
					VALUES (
					'".$_POST['id']."',
					'".$_POST['name']."',
					'".$_POST['title']."',
					'".$_POST['occupation']."',
					'".$_POST['body']."',
					'".$_POST['email']."',
					'".$_POST['fax']."',
					'".$_POST['phone']."',
					'".$_POST['mobile']."',
					'".$_POST['website']."',
					'/".$newname."',
					'".$position."')");
				}
			}
		}else{
			//update database

			mysql_query("INSERT INTO team
			(id, name, title, occupation, cv, email, fax, phone, mobile, website, image, position)
			VALUES (
			'".$_POST['id']."',
			'".$_POST['name']."',
			'".$_POST['title']."',
			'".$_POST['occupation']."',
			'".$_POST['body']."',
			'".$_POST['email']."',
			'".$_POST['fax']."',
			'".$_POST['phone']."',
			'".$_POST['mobile']."',
			'".$_POST['website']."',
			'',
			'".$position."')");
		}
		//show added page
		show_team_page("SELECT * FROM team WHERE id='".$_POST['id']."'");

	// UPDATE EDIT
	}elseif($_POST['task'] == 'edit'){
			 	$image=$_FILES['image']['name'];
			 	if($image){
					//get name
			 		$filename = stripslashes($_FILES['image']['name']);
			  		$extension = getExtension($filename);
			 		$extension = strtolower($extension);
					//Verify
					if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){
						echo '<h1>Unknown extension!</h1>';
						$errors=1;
					}else{
						$image_name=time().'.'.$extension;
						$newname="bilder/".$image_name;
						$copied = copy($_FILES['image']['tmp_name'], $newname);
						if (!$copied){
							echo '<h1>Copy unsuccessfull!</h1>';
							$errors=1;
						}
						if(!$errors){
							include 'include/resize.php';
							$image = new SimpleImage();
							$image->load($newname);
							$image->resizeToWidth(180);
							$image->save($newname);

							mysql_query("UPDATE team SET
							name = '".$_POST['name']."',
							title = '".$_POST['title']."',
							occupation = '".$_POST['occupation']."',
							cv = '".$_POST['body']."',
							email = '".$_POST['email']."',
							fax = '".$_POST['fax']."',
							phone = '".$_POST['phone']."',
							mobile = '".$_POST['mobile']."',
							website = '".$_POST['website']."',
							image = '/".$newname."'
							WHERE id = '".$_POST['id']."'");
						}
					}
				}else{
					if($_POST['rm_img'] == 1){
						mysql_query("UPDATE team SET
						name = '".$_POST['name']."',
						title = '".$_POST['title']."',
						occupation = '".$_POST['occupation']."',
						cv = '".$_POST['body']."',
						email = '".$_POST['email']."',
						fax = '".$_POST['fax']."',
						phone = '".$_POST['phone']."',
						mobile = '".$_POST['mobile']."',
						website = '".$_POST['website']."',
						image = ''
						WHERE id = '".$_POST['id']."'");
					}else{
						mysql_query("UPDATE team SET
						name = '".$_POST['name']."',
						title = '".$_POST['title']."',
						occupation = '".$_POST['occupation']."',
						cv = '".$_POST['body']."',
						email = '".$_POST['email']."',
						fax = '".$_POST['fax']."',
						phone = '".$_POST['phone']."',
						mobile = '".$_POST['mobile']."',
						website = '".$_POST['website']."'
						WHERE id = '".$_POST['id']."'");
					}
				}
		//show edited page
		show_team_page("SELECT * FROM team WHERE id='".$_POST['id']."'");

	// UPDATE DELETION
	}elseif($_GET['task'] == 'delete'){
		if(isset($_GET['true'])){
			//delete
			mysql_query("DELETE FROM team WHERE id = '".$_GET['id']."'");
			//show latest page
			show_team_page("SELECT * FROM team ORDER BY id DESC limit 1");
		}else{
			$get = mysql_fetch_array(mysql_query("SELECT name FROM team WHERE id='".$_GET['id']."'"));
			//deletion confirmation
			echo '<div id="msg">
			Are you sure you want to delete "'.$get['name'].'"?<br />
			<a href="edit.php?type=team&do=update&task=delete&id='.$_GET['id'].'&true=1"><button type="button">Yes</button></a> 
			<a href="edit.php?type=team&do=view&id='.$_GET['id'].'"><button type="button">No</button></a>
			</div>';
		}
	}
}elseif($_GET['do'] == 'pos'){
	include 'sort.php';
}
echo '</div>';

// team NAVIGATION LIST
		echo '<div id="map"><ul>
		<li class="b">team</li>';
		$res = mysql_query("SELECT * FROM team ORDER BY position ASC");
		while($row = mysql_fetch_array($res)){
			echo '<li>'.$row['name'].'<br />(<a href="edit.php?type=team&do=view&id='.$row['id'].'">View</a>
			<a href="edit.php?type=team&do=edit&id='.$row['id'].'">Edit</a>			
			<a href="edit.php?type=team&do=update&task=delete&id='.$row['id'].'">X</a>)
			</li>';
		}
		echo '</ul></div>';
?>
