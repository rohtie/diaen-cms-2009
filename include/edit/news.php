<?php
	/***********************************
				NEWS
	***********************************/
		echo '<div id="right">';
		echo '<div id="header">News</div>';
		echo '<div id="content">';
		function show_news_page($query){
			$res = mysql_query($query);
			$row = mysql_fetch_array($res);
			if(!empty($row['image'])){
				echo '<h1>
				'.$row['header'].'
				(<a href="edit.php?type=news&do=edit&id='.$row['id'].'">Edit</a>
				<a href="edit.php?type=news&do=update&task=delete&id='.$row['id'].'">X</a>)
				</h1>
				<div style="float: left; clear: both;"><strong>Short summary:</strong><br />
				<img src="'.$row['image'].'" class="nimg" style="float: left;" />
				'.n2p($row['short_sum']).'</div>
				<div><strong>Content:</strong><br />'.n2p($row['content']).'</div>';
			}else{
				echo '<h1>
				'.$row['header'].'
				(<a href="edit.php?type=news&do=edit&id='.$row['id'].'">Edit</a>
				<a href="edit.php?type=news&do=update&task=delete&id='.$row['id'].'">X</a>)
				</h1>
				<div><strong>Short summary:</strong><br /> '.n2p($row['short_sum']).'</div>
				<div><strong>Content:</strong><br />'.n2p($row['content']).'</div>';
			}
		}
		
		// VIEW NEWS
		if($_GET['do'] == 'view'){
			if(!isset($_GET['id'])){
				show_news_page("SELECT * FROM news ORDER BY id DESC limit 1");
			}else{
				show_news_page("SELECT * FROM news WHERE id='".$_GET['id']."'");
			}

		// ADD NEWS
		}elseif($_GET['do'] == 'add'){
				$id_r = mysql_fetch_array(mysql_query("SELECT id FROM news ORDER BY id DESC limit 1"));
				$id = $id_r['id']+1;
				echo '<form method="post" action="edit.php?type=news&do=update" enctype="multipart/form-data">
					<h2>Header:</h2>
						<textarea class="head" name="head"></textarea>
					<h2>Add Image:</h2>
						<input type="file" id="upload" name="image">
					<h2>Photo taken by</h2>
						<textarea class="head" name="imageby"></textarea>
					<h2>Short summary:</h2>
						<textarea id="short" name="short"></textarea>
					<h2>Link to:</h2>
						<textarea class="head" name="linkto"></textarea>
					<h2>Full story:</h2>
						<textarea id="body" name="body"></textarea>
					<input id="submit" type="submit" value="submit" name="submit" />
					<input type="hidden" name="task" value="add" />
					<input type="hidden" name="id" value="'.$id.'" />
					<input type="hidden" name="date" value="'.date("d.m.y - H:i:s").'" />
				</form>';

		// EDIT NEWS
		}elseif($_GET['do'] == 'edit' && isset($_GET['id'])){
			$res = mysql_query("SELECT * FROM news WHERE id='".$_GET['id']."'");
			$row = mysql_fetch_array($res);
			//show edit form
			echo '<form method="post" action="edit.php?type=news&do=update" enctype="multipart/form-data">
				<h2>Header:</h2>
					<textarea class="head" name="head">'.$row['header'].'</textarea>
				<h2>Add new image:</h2>
					<input type="file" id="upload" name="image"> Remove current image: <input type="checkbox" name="rm_img" value="1" />
				<h2>Photo taken by</h2>
					<textarea class="head" name="imageby">'.$row['imageby'].'</textarea>
				<h2>Short summary:</h2>
					<textarea id="short" name="short">'.$row['short_sum'].'</textarea>
				<h2>Link to:</h2>
					<textarea class="head" name="linkto">'.$row['linkto'].'</textarea>
				<h2>Full story:</h2>
					<textarea id="body" name="body">'.$row['content'].'</textarea>
				<input id="submit" type="submit" value="submit" name="submit" />
				<input type="hidden" name="task" value="edit" />
				<input type="hidden" name="id" value="'.$row['id'].'" />
				<input type="hidden" name="img2rm" value="'.$row['image'].'" />
			</form>';

		// UPDATE NEWS
		}elseif($_GET['do'] == 'update'){

			// UPDATE ADD
			if($_POST['task'] == 'add'){
			 	$image=$_FILES['image']['name'];
			 	if($image){
					//get name
			 		$filename = stripslashes($_FILES['image']['name']);
			  		$extension = getExtension($filename);
			 		$extension = strtolower($extension);
					//Verify
					if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){
						echo '<h1>Unknown extension!</h1>';
						$errors=1;
					}else{
						$image_name=time().'.'.$extension;
						$newname="nimg/".$image_name;
						$copied = copy($_FILES['image']['tmp_name'], $newname);
						if (!$copied){
							echo '<h1>Copy unsuccessfull!</h1>';
							$errors=1;
						}
						if(!$errors){
							include 'include/resize.php';
							$image = new SimpleImage();
							$image->load($newname);
							$image->resizeToWidth(150);
							$image->save($newname);

							if(empty($_POST['linkto'])){
								$_POST['imageby'] = 'Unkown';
							}

							mysql_query("INSERT INTO news
							(id, header, short_sum, content, image, imageby, date_published, linkto)
							VALUES (
							'".$_POST['id']."',
							'".$_POST['head']."',
							'".$_POST['short']."',
							'".$_POST['body']."',
							'/".$newname."',
							'".$_POST['imageby']."',
							'".$_POST['date']."',
							'".$_POST['linkto']."')");
						}
					}
				}else{
					mysql_query("INSERT INTO news
					(id, header, short_sum, content, date_published, linkto)
					VALUES ('".$_POST['id']."',
					'".$_POST['head']."',
					'".$_POST['short']."',
					'".$_POST['body']."',
					'".$_POST['date']."',
					'".$_POST['linkto']."')");
				}
				//show new page
				show_news_page("SELECT * FROM news ORDER BY id DESC limit 1");

			// UPDATE EDIT
			}elseif($_POST['task'] == 'edit'){
			 	$image=$_FILES['image']['name'];
			 	if($image){
					//get name
			 		$filename = stripslashes($_FILES['image']['name']);
			  		$extension = getExtension($filename);
			 		$extension = strtolower($extension);
					//Verify
					if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){
						echo '<h1>Unknown extension!</h1>';
						$errors=1;
					}else{
						$image_name=time().'.'.$extension;
						$newname="nimg/".$image_name;
						$copied = copy($_FILES['image']['tmp_name'], $newname);
						if (!$copied){
							echo '<h1>Copy unsuccessfull!</h1>';
							$errors=1;
						}
						if(!$errors){
							if(empty($_POST['linkto'])){
								$_POST['imageby'] = 'Unkown';
							}

							include 'include/resize.php';
							$image = new SimpleImage();
							$image->load($newname);
							$image->resizeToWidth(150);
							$image->save($newname);

							mysql_query("UPDATE news SET 
							header = '".$_POST['head']."',
							short_sum = '".$_POST['short']."',
							content = '".$_POST['body']."',
							linkto = '".$_POST['linkto']."',
							imageby = '".$_POST['imageby']."',
							image = '/".$newname."'
							WHERE id = '".$_POST['id']."'");
						}
					}
				}else{
					if($_POST['rm_img']){
						mysql_query("UPDATE news 
						SET header = '".$_POST['head']."',
						short_sum = '".$_POST['short']."',
						content = '".$_POST['body']."',
						linkto = '".$_POST['linkto']."',
						imageby = '',
						image = ''
						WHERE id = '".$_POST['id']."'");
						unlink(ltrim($_POST['img2rm'],"/"));
					}else{
						mysql_query("UPDATE news 
						SET header = '".$_POST['head']."',
						short_sum = '".$_POST['short']."',
						content = '".$_POST['body']."',
						linkto = '".$_POST['linkto']."',
						imageby = '".$_POST['imageby']."'
						WHERE id = '".$_POST['id']."'");
						
					}
				}
				//show edited page
				show_news_page("SELECT * FROM news WHERE id='".$_POST['id']."'");

			// UPDATE DELETION
			}elseif($_GET['task'] == 'delete'){
				if(isset($_GET['true'])){
					//delete
					mysql_query("DELETE FROM news WHERE id = '".$_GET['id']."'");
					//show latest page
					show_news_page("SELECT * FROM news ORDER BY id DESC limit 1");
				}else{
					$get = mysql_fetch_array(mysql_query("SELECT header FROM news WHERE id='".$_GET['id']."'"));
					//deletion confirmation
					echo '<div id="msg">
					Are you sure you want to delete "'.$get['header'].'"?<br />
					<a href="edit.php?type=news&do=update&task=delete&id='.$_GET['id'].'&true=1">Yes</a> 
					<a href="edit.php?type=news&do=view&id='.$_GET['id'].'">No</a>
					</div>';
				}
			}
		}

		// NEWS NAVIGATION LIST
		echo '</div>';
		echo '<div id="map"><ul>
		<li class="b">News</li>';
		$res = mysql_query("SELECT * FROM news ORDER BY id DESC");
		while($row = mysql_fetch_array($res)){
			echo '<li>'.$row['header'].'<br />(<a href="edit.php?type=news&do=view&id='.$row['id'].'">View</a>
			<a href="edit.php?type=news&do=edit&id='.$row['id'].'">Edit</a>			
			<a href="edit.php?type=news&do=update&task=delete&id='.$row['id'].'">X</a>)
			</li>';
		}
		echo '</ul></div>';
?>
