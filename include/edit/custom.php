<?php
echo '<div id="right">';
echo '<div id="header">Custom</div>';
echo '<div id="content">';
//scan directory
$directory = dir("custom");
while ($entry = $directory->read()) {
	if (preg_match("/(\.php$)/i", $entry)){
		$filename[] = $entry;
		$filepath[] = 'custom/'.$entry;
	}
}
$directory->close();

switch($_GET['do']){
//-- NEW --//
case 'new';
	if($_GET['c']){
		$handle = fopen('custom/'.$_POST['filename'], "w");
		fwrite($handle, $_POST['code']);
		fclose($handle);
		echo highlight_string($_POST['code'], ENT_QUOTES);
	}else{
		echo '<form action="edit.php?type=custom&do=new&c=1" method="post">
		<h2>Filename</h2>
		<input type="text" name="filename" class="head" />
		<h2>Code</h2>
		<textarea style="width: 99%; height: 430px;" name="code"></textarea>
		<input type="submit" id="submit" value="submit" />
		</form>';
	}
break;

//--  VIEW  --//
case 'view';
	if(!isset($_GET['id'])){
		$_GET['id'] = 0;
	}
	$handle = fopen($filepath[$_GET['id']], "r");
	$contents = fread($handle, filesize($filepath[$_GET['id']]));
	echo highlight_string($contents, ENT_QUOTES);
	fclose($handle);
break;

//--  EDIT  --//
case 'edit';
	if($_GET['c']){
		$handle = fopen($_POST['filename'], "w");
		fwrite($handle, $_POST['code']);
		fclose($handle);
		echo highlight_string($_POST['code'], ENT_QUOTES);
	}else{
		$handle = fopen($filepath[$_GET['id']], "r");
		$contents = fread($handle, filesize($filepath[$_GET['id']]));
		echo '<form action="edit.php?type=custom&do=edit&id='.$_GET['id'].'&c=1" method="post">
		<textarea style="width: 99%; height: 520px;" name="code">'.$contents.'</textarea><input type="submit" name="submit" id="submit" value="submit" />
		<input type="hidden" name="filename" value="'.$filepath[$_GET['id']].'" />
		</form>';
		fclose($handle);
	}
break;

//-- DELETE --//
case 'delete';
	if($_GET['c']){
		if(unlink($filepath[$_GET['id']])){
			echo '<div id="msg">"'.$filename[$_GET['id']].'" was successfully deleted<br /><a href="?type=custom&do=view">back</a></div>';
		}else{
			echo '<div id="msg">An error occured while attempting to delete "'.$filename[$_GET['id']].'"<br /><a href="?type=custom&do=view">back</a>
			</div>';			
		}
	}else{
		echo '<div id="msg">
		Are you sure you want to delete "'.$filename[$_GET['id']].'"<br />
		<a href="edit.php?type=custom&do=delete&id='.$_GET['id'].'&c=1">Yes</a> 
		<a href="edit.php?type=custom&do=view">No</a>
		</div>';
	}
break;
}

//NULL thar var
$filename = NULL;
//scan directory
$directory = dir("custom");
while ($entry = $directory->read()) {
	if(preg_match("/(\.php$)/i", $entry)){
		$filename[] = $entry;
	}
}
$directory->close();

echo '</div>';
echo '<div id="map"><ul>
<li class="b">Custom</li>';
for($i=0; $i < count($filename); $i++){
	echo '<li>'.$filename[$i].'<br />(<a href="edit.php?type=custom&do=view&id='.$i.'">View</a>
	<a href="edit.php?type=custom&do=edit&id='.$i.'&c=0">Edit</a>
	<a href="edit.php?type=custom&do=delete&id='.$i.'&c=0">X</a>)
	</li>';
}
echo '</ul></div>';
?>
