SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE `article` (
  `id` int(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `category` int(3) NOT NULL,
  `position` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `category` (
  `id` int(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `islink` int(1) NOT NULL,
  `article` int(6) NOT NULL,
  `site` int(2) NOT NULL,
  `position` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `news` (
  `id` int(5) NOT NULL,
  `header` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `short_sum` text NOT NULL,
  `image` varchar(256) NOT NULL,
  `date_published` varchar(256) NOT NULL,
  `linkto` varchar(256) NOT NULL,
  `imageby` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `site` (
  `id` int(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` int(2) NOT NULL,
  `content` text NOT NULL,
  `custom` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `islink` tinyint(1) NOT NULL,
  `include` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `team` (
  `id` int(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(256) NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `cv` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `position` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
