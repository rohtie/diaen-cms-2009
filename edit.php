<?php
include 'include/DB.php';
include 'include/functions.php';
echo
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="css/edit.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body><div id="main">
<div id="left"><ul>
<li class="b">News</li>
	<li><a href="?type=news&do=add" class="s">Add new</a></li>
	<li><a href="?type=news&do=view" class="s">View/Edit</a></li>
<li class="b">Images</li>
	<li><a href="?type=image&do=add" class="s">Upload</a></li>
	<li><a href="?type=image&do=view" class="s">View/edit</a></li>
<li class="b">Team</li>
	<li><a href="?type=team&do=add" class="s">New member</a></li>
	<li><a href="?type=team&do=view" class="s">View/Edit</a></li>
	<li><a href="?type=team&do=pos&cat=0" class="s">Rearrange</a></li>
<li class="b">Article</li>
	<li><a href="?type=article&do=add" class="s">Add new</a></li>
	<li><a href="?type=article&do=view" class="s">View/Edit</a></li>
	<li><a href="?type=article&do=pos&cat=1" class="s">Rearrange</a></li>
<li class="b">Category</li>
	<li><a href="?type=category&do=add" class="s">Add new</a></li>
	<li><a href="?type=category&do=view" class="s">View/Edit</a></li>
	<li><a href="?type=category&do=pos&cat=1&x=site" class="s">Rearrange</a></li>
<li class="b">Site</li>
	<li><a href="?type=site&do=add" class="s">Add new</a></li>
	<li><a href="?type=site&do=view" class="s">View/Edit</a></li>
	<li><a href="?type=site&do=pos&cat=0" class="s">Rearrange</a></li>
<li class="b">Custom Site</li>
	<li><a href="?type=custom&do=new&c=0" class="s">New</a></li>
	<li><a href="?type=custom&do=view" class="s">View/Edit</a></li>
</ul></div>';

//show admin tools
if(isset($_GET['type']) && isset($_GET['do'])){
	if($_GET['type'] == 'news'){
		include 'include/edit/news.php';

	}elseif($_GET['type'] == 'article'){
		include 'include/edit/article.php';

	}elseif($_GET['type'] == 'category'){
		include 'include/edit/category.php';

	}elseif($_GET['type'] == 'site'){
		include 'include/edit/site.php';

	}elseif($_GET['type'] == 'team'){
		include 'include/edit/team.php';

	}elseif($_GET['type'] == 'image'){
		include 'include/edit/image.php';

	}elseif($_GET['type'] == 'custom'){
		include 'include/edit/custom.php';
	}
}else{
	$_GET['type'] = 'news';
	$_GET['do'] = 'view';
	include 'include/edit/news.php';
}
//end of main
echo '</div></body></html>';
//disconnect
mysql_close();
?>
